# Goal

This image aims to provide a good lightweight image for CIs to execute simple 
jobs that require ansible and other lightweight tools.

# Packages

- curl
- zip
- open-ssh-client
- ansible-core
